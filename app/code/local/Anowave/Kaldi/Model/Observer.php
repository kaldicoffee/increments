<?php
/**
 * Anowave Kaldi
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Kaldi
 * @copyright 	Copyright (c) 2018 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */

class Anowave_Kaldi_Model_Observer
{
	/**
	 * Transport layer modifier 
	 * 
	 * @param Varien_Event_Observer $observer
	 */
	public function modify(Varien_Event_Observer $observer)
	{
		$content = $observer->getTransport()->getHtml();
		
		if (false !== strpos($content, 'value="25"'))
		{
			
		}
		return true;
	}
}