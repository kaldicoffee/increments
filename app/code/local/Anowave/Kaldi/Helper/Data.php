<?php
/**
 * Anowave Kaldi
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Anowave license that is
 * available through the world-wide-web at this URL:
 * http://www.anowave.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category 	Anowave
 * @package 	Anowave_Kaldi
 * @copyright 	Copyright (c) 2018 Anowave (http://www.anowave.com/)
 * @license  	http://www.anowave.com/license-agreement/
 */

class Anowave_Kaldi_Helper_Data extends Mage_Core_Helper_Abstract
{
	/**
	 * Default increment 
	 * 
	 * @var integer
	 */
	const DEFAULT_INCREMENT = 5; 
	
	/**
	 * Get increment QTY
	 * 
	 * @param Mage_Catalog_Model_Product $product
	 */
	public function getQty(Mage_Catalog_Model_Product $product)
	{
		$qty = (int) $product->getQtyIncrement();
		
		if (!$qty)
		{
			$qty = self::DEFAULT_INCREMENT;
		}
		
		return $qty;
	}
	
	public function getLbl(Mage_Catalog_Model_Product $product)
	{
		return $product->getQtyLabel();
	}
}